/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package observer

import "fmt"

/*
	观察者模式
*/

type Customer interface {
	update()
}

// 被观察A
type A struct{}

func (*A) update() {
	fmt.Println("A update")
}

// 被观察B
type B struct{}

func (*B) update() {
	fmt.Println("A update")
}

type NewsOffice struct {
	customers []Customer
}

func (n *NewsOffice) addCustomer(c Customer) {
	n.customers = append(n.customers, c)
}

func (n *NewsOffice) NoticeAll() {
	for _, customer := range n.customers {
		customer.update()
	}
}
