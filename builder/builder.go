/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package builder

type Book struct {
	ID    int
	Name  string
	Price float32
}

func (b *Book) Builder(id int, name string) *Builder {
	return NewBuilder(id, name)
}

func (b *Book) String() string {
	return "book"
}

type Builder struct {
	id    int
	name  string
	price float32
}

func NewBuilder(id int, name string) *Builder {
	return &Builder{
		id:   id,
		name: name,
	}
}

func (b *Builder) SetPrice(p float32) *Builder {
	b.price = p
	return b
}

func (b *Builder) Build() *Book {
	book := &Book{
		ID:   b.id,
		Name: b.name,
	}
	if b.price > 0 {
		book.Price = b.price
	}
	return book
}
