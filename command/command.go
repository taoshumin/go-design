/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package command

import (
	"fmt"
	"log"
)

type Commander interface {
	Exec(args ...interface{}) error
}

type Usr struct{}

func (u *Usr) Login(name, password string) error {
	log.Printf("name: %s password: %s", name, password)
	return nil
}

type LoginCmd struct {
	*Usr
}

func NewLoginCmd(usr *Usr) *LoginCmd {
	return &LoginCmd{usr}
}

func (l *LoginCmd) Exec(args ...interface{}) error {
	if len(args) != 2 {
		return fmt.Errorf("args must be equal 2")
	}
	return l.Login(args[0].(string), args[1].(string))
}

type Invoker struct {
	cmds []Commander
}

func (i *Invoker) Do(args ...interface{}) {
	for _, cmd := range i.cmds {
		err := cmd.Exec(args...)
		if err != nil {
			log.Printf("cmd exec error :%s", err)
		}
	}
}

func (i *Invoker) Append(cmd ...Commander) {
	i.cmds = append(i.cmds, cmd...)
}
