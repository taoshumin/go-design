## slice delete 

Copy from github.com/syncthing/lib/config/wrapper.go
```
// Unsubscribe de-registers the given handler from any future calls to
// configuration changes and only returns after a potential ongoing config
// change is done.
func (w *wrapper) Unsubscribe(c Committer) {
	w.mut.Lock()
	for i := range w.subs {
		if w.subs[i] == c {
			copy(w.subs[i:], w.subs[i+1:])
			w.subs[len(w.subs)-1] = nil
			w.subs = w.subs[:len(w.subs)-1]
			break
		}
	}
	waiter := w.waiter
	w.mut.Unlock()
	// Waiting mustn't be done under lock, as the goroutines in notifyListener
	// may dead-lock when trying to access lock on config read operations.
	waiter.Wait()
}
```