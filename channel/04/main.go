/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"time"
)

func solution(i int) {
	ch := make(chan int)
	go func(i int) {
		result := do(i)
		time.Sleep(6 * time.Second) // 6秒后退出
		ch <- result
	}(i)

	select {
	case resp := <-ch: // case 没有default,阻塞等待
		fmt.Println("recive: ", resp)
	}
	fmt.Println("exit 0")
}

func do(i int) int {
	return i
}

func main() {
	solution(10)
	time.Sleep(5 * time.Second)
	fmt.Println("exit 1")
}

// 结论：select有default与没default区别
