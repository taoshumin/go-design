/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int, 1)
	go solution(ch)

	time.Sleep(1 * time.Minute)
	ch <- 1
}

func solution(ch chan int) {
	for {
		select {
		case <-ch:
			return
		default:
		}

		fmt.Println("1111")
	}
}

// 测试 default与没有default区别
