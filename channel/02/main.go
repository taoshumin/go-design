/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"time"
)

type Conn struct {
	name string
}

func (c Conn) Do(q string) string {
	return c.name
}

func solution(conns []Conn, query string) string {
	ch := make(chan string, 1)
	for _, conn := range conns {
		go func(c Conn) {
			select {
			case ch <- c.Do(query):
			}
		}(conn)
	}
	return <-ch
}

func main() {
	// Create fake data
	conns := make([]Conn, 0, 10)
	for i := 0; i < 10; i++ {
		conns = append(conns, Conn{name: fmt.Sprintf("%d", i)})
	}

	r := solution(conns, "connects")
	fmt.Println("recive: ", r)
	time.Sleep(5 * time.Second)
}

// 总结：Query 函数获取数据库的连接切片并请求。并行请求每一个数据库并返回收到的第一个响应.
// 只返回最先收到的数据.
