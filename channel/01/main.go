/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import "fmt"

func solution(n int, ch chan<- int) {
	for i := 0; i < n; i++ {
		ch <- i
	}
	close(ch)
}

func main() {
	ch := make(chan int, 1)
	go solution(1000, ch)
	for r := range ch {
		fmt.Println("revice: ", r)
	}
}

// 总结：使用for-range读取channel返回的结果十分便利。当channel关闭且没有数据时，for循环会自动退出，
// 无需主动监测channel是否关闭。close(ch)只针对写数据到channel起作用，意思是close(ch)后，ch中不能再写数据，但不影响从ch中读数据
