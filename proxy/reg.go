/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package proxy

import "fmt"

// copy from prometheus-go

type Gatherer interface {
	Gather() error
}

type Registry struct{}

func (r *Registry) Gather() error {
	fmt.Println("Gather")
	return nil
}

type Registerer interface {
	Register() error
	MustRegister() error
}

type Supper struct{}

// 如果要想Supper实现接口Gatherer 和 Registerer 则必须返回 结构体指针
func NewSuper() *Supper {
	return &Supper{}
}

func (s *Supper) Register() error {
	fmt.Println("Register")
	return nil
}

func (s *Supper) MustRegister() error {
	fmt.Println("MustRegister")
	return nil
}

func (s *Supper) Gather() error {
	fmt.Println("Supper")
	return nil
}
