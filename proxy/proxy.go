/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package proxy

import "log"

type Usr struct{}

func (u *Usr) Login(name, password string) {
	log.Printf("name: %s password: %s sucess", name, password)
}

type UsrProxy struct {
	usr *Usr
}

func NewUsrProxy(usr *Usr) *UsrProxy {
	return &UsrProxy{usr: usr}
}

func (u *UsrProxy) Login(name, password string) {
	log.Printf("before usr proxy")
	u.usr.Login(name, password)
	log.Printf("after usr proxy")
}
