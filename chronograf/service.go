/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package chronograf

var _ KVClient = (*Service)(nil)

type KVClient interface {
	DashboardsStore() DashboardsStore
	UserStore() UserStore
}

type Service struct {
	kv Store
}

// NewService return 注意，返回的是*Service
// 方便后面Close
func NewService(kv Store) *Service {
	return &Service{kv: kv}
}

// Close closes the service's kv store.
func (s *Service) Close() error {
	return s.kv.Close()
}

func (s *Service) DashboardsStore() DashboardsStore {
	return &dashboards{client: s}
}

func (s *Service) UserStore() UserStore {
	return &user{client: s}
}

// 总结: 最后汇总到一个结构体内
