/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package chronograf

type Store interface {
	View() error
	Update() error
	Close() error
}

type client struct{}

// NewClient 返回的是接口实现
func NewClient() Store {
	return &client{}
}

func (c client) View() error {
	panic("implement me")
}

func (c client) Update() error {
	panic("implement me")
}

func (c client) Close() error {
	panic("implement me")
}
