/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package vec

import "fmt"

type Collector interface {
	Describe()
	Collect()
}

type metricMap struct{}

func (m *metricMap) Describe() {
	fmt.Println("describe")
}

func (m *metricMap) Collect() {
	fmt.Println("Collect")
}

type MetricVec struct {
	*metricMap
}

func NewMetricVec() *MetricVec {
	return &MetricVec{
		&metricMap{},
	}
}

// 重写接口实现
func (m *MetricVec) Describe() {
	m.metricMap.Describe()
	fmt.Println("describe")
}

func (m *MetricVec) Collect() {
	m.metricMap.Collect()
	fmt.Println("Collect")
}

type CounterVec struct {
	*MetricVec
}

func (c *CounterVec) Add() {
	fmt.Println("add")
}
