/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package edge

import "time"

type GroupID string

const (
	NilGroup GroupID = ""
)

type MessageType int

const (
	Barrier MessageType = iota
	DeleteGroup
)

// Message an interface that all messages must implement,
// similar to the parent class in inheritance.
type Message interface {
	Type() MessageType
}

// NameGetter is the barrier message.
type NameGetter interface {
	Name() string
}

type BarrierMessage interface {
	Message

	NameGetter
	ShallowCopy() BarrierMessage
}

type barrierMessage struct {
	group GroupID
	time  time.Time
}

func NewBarrierMessage(group GroupID, time time.Time) BarrierMessage {
	return &barrierMessage{group: group, time: time}
}

func (b *barrierMessage) Type() MessageType {
	return Barrier
}

func (b *barrierMessage) Name() string {
	return "barrier"
}

func (b *barrierMessage) ShallowCopy() BarrierMessage {
	c := new(barrierMessage)
	*c = *b
	return c
}

// GroupIDGetter is to delete message.
type GroupIDGetter interface {
	GroupID() GroupID
}

type DeleteGroupMessge interface {
	Message
	GroupIDGetter
}

type deleteGroupMessage struct {
	groupID GroupID
}

func NewDeleteGroupMessage(groupID GroupID) DeleteGroupMessge {
	return &deleteGroupMessage{groupID: groupID}
}

func (d *deleteGroupMessage) Type() MessageType {
	return DeleteGroup
}

func (d *deleteGroupMessage) GroupID() GroupID {
	return d.groupID
}
