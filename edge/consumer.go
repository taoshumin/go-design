/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package edge

import "fmt"

type Receiver interface {
	Barrier(b BarrierMessage) error
	DeleteGroup(d DeleteGroupMessge) error

	Done()
}

// Consumer reads messages off an edge and passes them to a receiver.
type Consumer interface {
	// Consume reads messages off an edge until the edge is closed or aborted.
	// An error is returned if either the edge or receiver errors.
	Consume() error
}

type consumer struct {
	message Message
	r       Receiver
}

func NewConsumer(r Receiver) Consumer {
	return &consumer{r: r}
}

func (ec *consumer) Consume() error {
	defer ec.r.Done()

	switch m := ec.message.(type) {
	case BarrierMessage:
		if err := ec.r.Barrier(m); err != nil {
			return err
		}
	case DeleteGroupMessge:
		if err := ec.r.DeleteGroup(m); err != nil {
			return err
		}
	default:
		return fmt.Errorf("unexpected message of type %T", m)
	}
	return nil
}
