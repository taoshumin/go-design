/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package edge

type ForwardReceiver interface {
	Barrier(b BarrierMessage) (Message, error)
	DeleteGroup(d DeleteGroupMessge) (Message, error)

	// Done is called once the receiver will no longer receive any messages.
	Done()
}

//
type ForwardBufferedReceiver interface {
	ForwardReceiver
	BufferedBatch(batch []byte) (Message, error)
}

type forwardingReceiver struct {
	r ForwardReceiver
}

func NewReceiverFromForwardReceiver(r ForwardReceiver) Receiver {
	b, ok := r.(ForwardBufferedReceiver)
	if ok{
		return &forwardingBufferedReceiver{
			forwardingReceiver: forwardingReceiver{
				r:    r,
			},
			b: b,
		}
	}
	return &forwardingReceiver{
		r: r,
	}
}

type forwardingBufferedReceiver struct {
	forwardingReceiver
	b ForwardBufferedReceiver
}

func (fr *forwardingBufferedReceiver) BufferedBatch(batch []byte) (Message, error){
	return nil, nil
}

func (fr *forwardingReceiver) Barrier(b BarrierMessage) error {
	return fr.forward(fr.r.Barrier(b))
}

func (fr *forwardingReceiver) DeleteGroup(d DeleteGroupMessge) error {
	return fr.forward(fr.r.DeleteGroup(d))
}

func (fr *forwardingReceiver) Done() {
	fr.r.Done()
}

func (fr *forwardingReceiver) forward(msg Message, err error) error {
	return nil
}
