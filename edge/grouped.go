/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package edge

// 实现的一种形式
type GroupedConsumer interface {
	Consumer

	CardinalityVar() error
}

type groupedConsumer struct {
	consumer Consumer
	current  Receiver
}

func NewGroupedConsumer() GroupedConsumer {
	gc := &groupedConsumer{}

	// 很奇葩的一种写发
	// 但是很喜欢这种写法
	gc.consumer = NewConsumer(gc)
	return gc
}

func (c *groupedConsumer) CardinalityVar() error {
	return nil
}

func (c *groupedConsumer) Consume() error {
	return c.consumer.Consume()
}

func (c *groupedConsumer) Barrier(b BarrierMessage) error {
	return nil
}

func (c *groupedConsumer) DeleteGroup(d DeleteGroupMessge) error {
	return nil
}

func (c *groupedConsumer) Done() {}
