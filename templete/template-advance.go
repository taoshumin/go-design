/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package templete

import "fmt"

type Cooker interface {
	openfire()
	cooke()
	outfire()
	closed()
}

// 定义一个抽象类
// 开火，关火，关闭煤气 都是相同的
// 除了做饭不同
type CookSub struct{}

func (c CookSub) openfire() {
	fmt.Println("open fire")
}

func (c CookSub) cooke() {}

func (c CookSub) outfire() {
	fmt.Println("outfire")
}

func (c CookSub) closed() {
	fmt.Println("closed")
}

// 封装具体步骤
func doCook(cook Cooker) {
	cook.outfire()
	cook.cooke()
	cook.outfire()
	cook.closed()
}

// 炒西红柿
type XihongShi struct {
	CookSub
}

func (x XihongShi) cooke() {
	fmt.Println("西红柿")
}

// 饺子
type Jiaozi struct {
	CookSub
}

func (j Jiaozi) cooke() {
	fmt.Println("饺子")
}

/*
	总结：
	1. 先定义抽象类，把相同的实现
	2. 模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤
*/

// 重点： 比较常用
// 相当于方法重载，子类重写父类的方法
