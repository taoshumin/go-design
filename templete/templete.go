/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package templete

type Cacher interface {
	Get() interface{}
	Set() interface{}
}

type Cache struct {
	c Cacher
}

func (c *Cache) Get() interface{} {
	return nil
}

func (c *Cache) Set() interface{} {
	return nil
}

type Redis struct {
	*Cache
}

func NewRedis() *Redis {
	r := &Redis{}
	r.Cache = &Cache{c: r}
	return r
}

func (r *Redis) Get() interface{} {
	return "redis"
}

type File struct {
	*Cache
}

func NewFile() *File {
	f := &File{}
	f.Cache = &Cache{c: f}
	return f
}

func (r *File) Get() interface{} {
	return "file"
}
