## [design](https://www.jtthink.com/course/play/3536)

简介：常用设计模式，[简介学习](https://lailin.xyz/post/go-design-pattern.html)

## channel

简介：常用的goroutine channel使用


## Protobuf安装

[https://learnku.com/articles/39972](https://learnku.com/articles/39972)


## 编译时加入版本信息

```go
var (
    gitHash   string
    buildTime string
    goVersion string
)
```


```shell
go build -ldflags "-X 'main.goVersion=$(go version)' -X 'main.gitHash=$(git show -s --format=%H)' -X 'main.buildTime=$(git show -s --format=%cd)'" -o main.exe version.go
```


## go mod v2版本引用

[https://learnku.com/articles/47142](https://learnku.com/articles/47142)