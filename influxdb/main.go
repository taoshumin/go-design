/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
)

type Label struct {
	name string
	id   string
}

type LabelService interface {
	FindByID(ctx context.Context, id string) ([]*Label, error)
	Delete(ctx context.Context, id string) error
}

type AuthLabel struct{}

func NewAuthLabel() LabelService {
	return &AuthLabel{}
}

func (a *AuthLabel) FindByID(ctx context.Context, id string) ([]*Label, error) {
	fmt.Println("auth label find by id")
	return nil, nil
}

func (a *AuthLabel) Delete(ctx context.Context, id string) error {
	fmt.Println("auth label delete by id")
	return nil
}

type LogLabel struct {
	s LabelService
}

func NewLogLabel(s LabelService) LabelService {
	return &LogLabel{
		s: s,
	}
}

func (l *LogLabel) FindByID(ctx context.Context, id string) ([]*Label, error) {
	fmt.Println("log label find by id")
	return l.s.FindByID(ctx, id)
}

func (l *LogLabel) Delete(ctx context.Context, id string) error {
	fmt.Println("log delete by id")
	return l.s.Delete(ctx, id)
}

func main() {
	var labelSvc LabelService
	labelSvc = NewAuthLabel()
	labelSvc = NewLogLabel(labelSvc)
	_, _ = labelSvc.FindByID(context.Background(), "id")
}
