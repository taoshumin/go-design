/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package strategy

/*
	策略模式
*/
type IStrategy interface {
	do(int, int) int
}

// 加
type add struct{}

func (*add) do(a int, b int) int {
	return a + b
}

// 减
type reduce struct{}

func (*reduce) do(a int, b int) int {
	return a - b
}

// 具体策略执行者
type Operator struct {
	strategy IStrategy
}

func (o Operator) Set(strategy IStrategy) {
	o.strategy = strategy
}

func (o Operator) calculate(a, b int) int {
	return o.strategy.do(a, b)
}

/*
	总结：
	1. 定义好方法实现
	2. 使他们相互可替换
*/
