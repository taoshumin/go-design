/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package status

import "log"

type Status interface {
	Next(dell *Dell) error
}

type Dell struct {
	status Status
}

func NewDell(status Status) *Dell {
	return &Dell{status: status}
}

func (d *Dell) SetStatus(status Status) {
	d.status = status
}

func (d *Dell) Next() error {
	return d.status.Next(d)
}

type Usr struct{}

func (u *Usr) Next(dell *Dell) error {
	log.Printf("usr next")
	dell.SetStatus(&License{})
	return nil
}

type License struct{}

func (l *License) Next(dell *Dell) error {
	log.Printf("license next")
	return nil
}
