/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package decoratoer

import "log"

type Usr struct {
	id   int
	name string
}

type UsrFunc func(usr *Usr)

func WithUsrID(id int) UsrFunc {
	return func(usr *Usr) {
		usr.id = id
	}
}

func WithUsrName(n string) UsrFunc {
	return func(usr *Usr) {
		usr.name = n
	}
}

func NewUsr(opts ...UsrFunc) *Usr {
	u := &Usr{}

	for _, opt := range opts {
		opt(u)
	}
	return u
}

func (u *Usr) Do() {
	log.Printf("do")
}
